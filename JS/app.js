
// Знайти всі параграфи на сторінці та встановити колір фону #ff0000
const par = document.querySelectorAll('p');
console.log(par);

par.forEach(par => {
    par.style.backgroundColor = '#ff0000'
});





// Знайти елемент із id="optionsList". Вивести у консоль. Знайти батьківський елемент та вивести в консоль. Знайти дочірні ноди, якщо вони є, і вивести в консоль назви та тип нод.
const someList = document.getElementById('optionsList');
console.log(someList);

console.log(someList.parentNode);

console.log(someList.childNodes);


// Встановіть в якості контента елемента з класом (в самом html документе указан id, а не класс) testParagraph наступний параграф - This is a paragraph
const newPar = document.querySelector('#testParagraph');
newPar.innerHTML = '<p>This is a paragraph</p>';


// Отримати елементи, вкладені в елемент із класом main-header і вивести їх у консоль. Кожному з елементів присвоїти новий клас nav-item.
const newEl = document.querySelector('.main-header');
console.log(newEl.children);

for (let i = 0; i < newEl.children.length; i++) {
    newEl.children[i].classList.add('nav-item');
    
}

console.log(newEl.children) // collection



// Знайти всі елементи із класом section-title. Видалити цей клас у цих елементів.
const delClass = document.querySelectorAll('.section-title')
console.log(delClass);

delClass.forEach(delClass => {
    delClass.classList.remove('section-title');
});


console.log(delClass);